using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agent1 : MonoBehaviour
{
    // Start is called before the first frame update
    public float Speed = 4f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A)) transform.Translate(-Speed*Time.deltaTime,0,0);
        if(Input.GetKey(KeyCode.D)) transform.Translate(Speed*Time.deltaTime,0,0);
        if(Input.GetKey(KeyCode.W)) transform.Translate(0,Speed*Time.deltaTime,0);
        if(Input.GetKey(KeyCode.S)) transform.Translate(0,-Speed*Time.deltaTime,0);
    }

    private void OncollicsionEnter2D(Collision2D other)
    {
        
    }
}
