using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum GhostNodeStatesEnum
    {
        leftNode,
        rightNode,
        centerNode,
        startNode,
        movingInNodes
    }
    public GhostNodeStatesEnum ghostNodeState;

    public enum GhostType{
        red,blue
    }
    
    public GhostType ghostType;
    public GameObject ghostNodeLeft;
    public GameObject ghostNodeRight;
    public GameObject ghostNodeCenter;
    public GameObject ghostNodeStart;
    private MovementController movementController;
    public GameObject startingNode;
    public bool readytoLeaveHome;
    public GameManager gameManager;
    private float totalTimeEnemy = 0f;
    private string direction2;


    void Awake() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        movementController = GetComponent<MovementController>();
        if(ghostType == GhostType.red)
        {
            ghostNodeState = GhostNodeStatesEnum.rightNode;
            startingNode = ghostNodeRight;
        }
        else if(ghostType == GhostType.blue)
        {
            ghostNodeState = GhostNodeStatesEnum.leftNode;
            startingNode = ghostNodeLeft;

        }
        movementController.currentNode = startingNode;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ReadytoLeaveHome();
    }

    public void ReachedCenterOfNode(Pellet nodeController)
    {
        if(ghostNodeState == GhostNodeStatesEnum.movingInNodes)
        {
            if(ghostType == GhostType.red)
            {
                DetermineRedGhostDirection();
            }
            else if (ghostType == GhostType.blue)
            {
                DetermineBlueGhostDirection();
            }
        }
        else if(readytoLeaveHome)
        {
            if(ghostNodeState == GhostNodeStatesEnum.leftNode)
            {
                ghostNodeState = GhostNodeStatesEnum.centerNode;
                movementController.setDirection("right");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.rightNode)
            {
                ghostNodeState = GhostNodeStatesEnum.centerNode;
                movementController.setDirection("left");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.centerNode)
            {
                ghostNodeState = GhostNodeStatesEnum.startNode;
                movementController.setDirection("up");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.startNode)
            {
                ghostNodeState = GhostNodeStatesEnum.movingInNodes;
                movementController.setDirection("left");
            }
        }
    }

    void DetermineRedGhostDirection ()
    {
        string direction = GetClosesDirection(gameManager.Agent.transform.position);
        movementController.setDirection(direction);
    }

    void DetermineBlueGhostDirection ()
    {
        if(true) 
        {
            direction2 = GetClosesDirection(gameManager.Agent.transform.position);
        }
        // else if (false)
        // {
        //     string direction2 = GetClosesDirection(gameManager.Agent2.transform.position);
        // }
        movementController.setDirection(direction2);
    }

    string GetClosesDirection ( Vector2 target)
    {
        float shortestDistance = 0f;
        string lastMovingDirection = movementController.lastMovingDirection;
        string newDirection = "";
        Pellet nodeController = movementController.currentNode.GetComponent<Pellet>();
        
        if(nodeController.canMoveUp && lastMovingDirection != "down")
        {
            GameObject nodeUp = nodeController.nodeUp;

            float Distance = Vector2.Distance(nodeUp.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "up";
            }
        }

        if(nodeController.canMoveDown && lastMovingDirection != "up")
        {
            GameObject nodeDown = nodeController.nodeDown;

            float Distance = Vector2.Distance(nodeDown.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "down";
            }
        }

        if(nodeController.canMoveLeft && lastMovingDirection != "right")
        {
            GameObject nodeLeft = nodeController.nodeLeft;

            float Distance = Vector2.Distance(nodeLeft.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "left";
            }
        }

        if(nodeController.canMoveRight && lastMovingDirection != "left")
        {
            GameObject nodeRight = nodeController.nodeRight;

            float Distance = Vector2.Distance(nodeRight.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "right";
            }
        }
        return newDirection;
    }

    void ReadytoLeaveHome()
    {
        totalTimeEnemy += Time.deltaTime;
        if(totalTimeEnemy > 2 && ghostType == GhostType.red)
        {
            readytoLeaveHome = true;
        }
        else if(totalTimeEnemy > 6 && ghostType == GhostType.blue)
        {
            readytoLeaveHome = true;
        }
        
    }
}
